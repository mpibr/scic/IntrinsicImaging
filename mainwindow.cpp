#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_audio(new AudioDriver(this))
    , m_iworker(new IntrinsicWorker())
    , m_iengine(new IntrinsicEngine(this))
    , m_camera(nullptr)
    , m_camera_usb(new CameraUsb())
    , m_camera_flir(new CameraFlir())
    , m_exporter(new FrameExporter())
    , m_threadCamera(new QThread(this))
    , m_threadIWorker(new QThread(this))
    , m_threadExporter(new QThread(this))
    , m_timerElapsed(new QTimer(this))
    , m_elapsedSeconds(0)
    , m_exportPath("")
{
    ui->setupUi(this);
    setupExport();
    setupUiWidgets();
    setupThreads();
    setupConnections();

    notify("gray", "IntrinsicImaging v1.0.0");

    emit cameraOpen();
    //emit cameraStart();
}

MainWindow::~MainWindow()
{
    delete ui;

    if (m_threadIWorker) {
        m_threadIWorker->quit();
        m_threadIWorker->wait();
    }

    if (m_threadCamera) {
        m_threadCamera->quit();
        m_threadCamera->wait();
    }

    if (m_threadExporter) {
        m_threadExporter->quit();
        m_threadExporter->wait();
    }
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    event->ignore();

    if (QMessageBox::Yes == QMessageBox::question(this, "Close confirmation", "Exit?", QMessageBox::Yes | QMessageBox::No)) {

        // clean export path
        if (QDir(m_exportPath).isEmpty())
            QDir(m_exportPath).removeRecursively();
        event->accept();
    }
}


void MainWindow::setupUiWidgets()
{
    // settings audio
    configureSpinBox(ui->spinBox_audioPulses, 1, 10, 1, 1);
    configureSpinBox(ui->spinBox_audioFreqStart, 100, 20000, 100, 1000);
    configureSpinBox(ui->spinBox_audioFreqEnd, 100, 20000, 100, 15000);
    configureSpinBox(ui->doubleSpinBox_audioDuration, 0.1, 10.0, 0.1, 1.0);
    configureSpinBox(ui->doubleSpinBox_audioOffset, 0.0, 10.0, 0.1, 0.0);

    // settings trial
    configureSpinBox(ui->doubleSpinBox_trialTimeIdle, 0.0, 120.0, 0.5, 4.0);
    configureSpinBox(ui->doubleSpinBox_trialTimeBaseline, 0.0, 120.0, 0.5, 2.0);
    configureSpinBox(ui->doubleSpinBox_trialTimePreCue, 0.0, 120.0, 0.5, 0.5);
    configureSpinBox(ui->doubleSpinBox_trialTimePostCue, 0.0, 120.0, 0.5, 0.5);
    configureSpinBox(ui->doubleSpinBox_trialTimeSignal, 0.0, 120.0, 0.5, 2.0);
    configureSpinBox(ui->doubleSpinBox_threshold, 0.0, 1.0, 0.1, 0.5);

    // settings threshold
    ui->horizontalSlider_threshold->setRange(0,100);
    ui->horizontalSlider_threshold->setSingleStep(1);
    ui->horizontalSlider_threshold->setValue(50);
}


void MainWindow::setupThreads()
{
    // intrinsic worker
    m_iworker->moveToThread(m_threadIWorker);
    connect(m_threadIWorker, &QThread::finished, m_iworker, &IntrinsicWorker::deleteLater);
    m_threadIWorker->start();

    // handle automatic camera device choice
    if (m_camera_flir->isDetected()) {
        m_camera = m_camera_flir;
        delete m_camera_usb;
    }
    else {
        m_camera = m_camera_usb;
        delete m_camera_flir;
    }

    m_camera->moveToThread(m_threadCamera);
    connect(m_threadCamera, &QThread::finished, m_camera, &CameraDevice::deleteLater);
    m_threadCamera->start();

    // video/image exporter
    m_exporter->moveToThread(m_threadExporter);
    connect(m_threadExporter, &QThread::finished, m_exporter, &FrameExporter::deleteLater);
    m_threadExporter->start();
}


void MainWindow::setupConnections()
{
    /* --- connections are sorted by signals --- */

    // mainwindow
    connect(this, &MainWindow::notify, ui->widget_eventLog, &EventLog::on_notify);
    connect(this, &MainWindow::audioLoad, m_audio, &AudioDriver::on_chirpLoad);
    connect(this, &MainWindow::audioCompose, m_audio, &AudioDriver::on_chirpCompose);
    connect(this, &MainWindow::audioPlay, m_audio, &AudioDriver::on_play);
    connect(this, &MainWindow::audioStop, m_audio, &AudioDriver::on_stop);
    connect(this, &MainWindow::engineStart, m_iengine, &IntrinsicEngine::engineStart);
    connect(this, &MainWindow::engineStop, m_iengine, &IntrinsicEngine::engineStop);
    connect(this, &MainWindow::cameraOpen, m_camera, &CameraDevice::on_open);
    connect(this, &MainWindow::cameraClose, m_camera, &CameraDevice::on_close);
    connect(this, &MainWindow::cameraStart, m_camera, &CameraDevice::on_start);
    connect(this, &MainWindow::cameraStop, m_camera, &CameraDevice::on_stop);
    connect(this, &MainWindow::flagHeatmap, m_iworker, &IntrinsicWorker::on_setFlagHeatmap);
    connect(this, &MainWindow::flagSaturation, m_iworker, &IntrinsicWorker::on_setFlagSaturation);
    connect(this, &MainWindow::takeSnapshot, ui->widget_viewer, &IntrinsicViewer::on_snapshot);

    // audiodriver
    connect(m_audio, &AudioDriver::notify, ui->widget_eventLog, &EventLog::on_notify);
    connect(m_audio, &AudioDriver::audioReady, m_iengine, &IntrinsicEngine::cueReady);

    // cameradevice
    connect(m_camera, &CameraDevice::notify, ui->widget_eventLog, &EventLog::on_notify);
    connect(m_camera, &CameraDevice::frameReady, m_iworker, &IntrinsicWorker::on_frame);
    //connect(m_camera, &CameraDevice::frameReady, ui->widget_viewer, &IntrinsicViewer::on_frame);

    // intrinsicengine
    connect(m_iengine, &IntrinsicEngine::notify, ui->widget_eventLog, &EventLog::on_notify);
    connect(m_iengine, &IntrinsicEngine::engineStart, m_iworker, &IntrinsicWorker::on_start);
    connect(m_iengine, &IntrinsicEngine::engineStart, ui->widget_chart, &IntrinsicChart::on_start);
    connect(m_iengine, &IntrinsicEngine::engineStop, ui->widget_chart, &IntrinsicChart::on_stop);
    connect(m_iengine, &IntrinsicEngine::cuePlay, m_audio, &AudioDriver::on_play);
    // --- cueReady
    connect(m_iengine, &IntrinsicEngine::flagBaseline, m_iworker, &IntrinsicWorker::on_setFlagBaseline);
    connect(m_iengine, &IntrinsicEngine::flagBaseline, ui->widget_chart, &IntrinsicChart::on_setFlagBaseline);
    connect(m_iengine, &IntrinsicEngine::flagCue, ui->widget_chart, &IntrinsicChart::on_setFlagCue);
    connect(m_iengine, &IntrinsicEngine::flagSignal, m_iworker, &IntrinsicWorker::on_setFlagSignal);
    connect(m_iengine, &IntrinsicEngine::flagSignal, ui->widget_chart, &IntrinsicChart::on_setFlagSignal);
    connect(m_iengine, &IntrinsicEngine::trialReady, m_iworker, &IntrinsicWorker::on_trial);

    // intrinsicviewer
    connect(ui->widget_viewer, &IntrinsicViewer::reportFrameRate, this, &MainWindow::on_frameRateUpdate);
    connect(ui->widget_viewer, &IntrinsicViewer::savePixmap, m_exporter, &FrameExporter::on_savePng);

    // intrinsicworker
    connect(m_iworker, &IntrinsicWorker::frameView, ui->widget_viewer, &IntrinsicViewer::on_frame);
    connect(m_iworker, &IntrinsicWorker::maskHeatmap, ui->widget_viewer, &IntrinsicViewer::on_maskHeatmap);
    connect(m_iworker, &IntrinsicWorker::maskSaturation, ui->widget_viewer, &IntrinsicViewer::on_maskSaturation);
    connect(m_iworker, &IntrinsicWorker::plotIntrinsic, ui->widget_chart, &IntrinsicChart::on_plotIntrinsic);
    connect(m_iworker, &IntrinsicWorker::saveFrame, m_exporter, &FrameExporter::on_saveTif);

    // ui signals
    connect(ui->checkBox_viewSaturation, &QCheckBox::stateChanged, [this](int state){emit flagSaturation(state == Qt::Checked);});
    connect(ui->checkBox_viewColormap, &QCheckBox::stateChanged, [this](int state){emit flagHeatmap(state == Qt::Checked);});
    connect(ui->doubleSpinBox_threshold, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            m_iworker, &IntrinsicWorker::on_setThreshold);
    connect(ui->doubleSpinBox_trialTimeIdle, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            m_iengine, &IntrinsicEngine::on_setTimeIdle);
    connect(ui->doubleSpinBox_trialTimeBaseline, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            m_iengine, &IntrinsicEngine::on_setTimeBaseline);
    connect(ui->doubleSpinBox_trialTimePreCue, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            m_iengine, &IntrinsicEngine::on_setTimePreCue);
    connect(ui->doubleSpinBox_trialTimePostCue, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            m_iengine, &IntrinsicEngine::on_setTimePostCue);
    connect(ui->doubleSpinBox_trialTimeSignal, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            m_iengine, &IntrinsicEngine::on_setTimeSignal);

    // private
    connect(m_timerElapsed, &QTimer::timeout, this, &MainWindow::on_timerElapsed);
}


void MainWindow::setupAudio()
{
    const QVector<AudioDriver::ChirpType> chirpType = {AudioDriver::ChirpType::Pure,
                                                       AudioDriver::ChirpType::Linear,
                                                       AudioDriver::ChirpType::Exponential};

    int pulses = ui->spinBox_audioPulses->value();
    qreal freqStart = static_cast<qreal>(ui->spinBox_audioFreqStart->value());
    qreal freqEnd = static_cast<qreal>(ui->spinBox_audioFreqEnd->value());
    qreal duration = ui->doubleSpinBox_audioDuration->value();
    qreal offset = ui->doubleSpinBox_audioOffset->value();
    AudioDriver::ChirpType type = chirpType.at(ui->comboBox_audioChirp->currentIndex());

    emit audioCompose(type, pulses, duration, offset, freqStart, freqEnd);
}


void MainWindow::setEnabledAudioUI(bool enabled)
{
    ui->spinBox_audioPulses->setEnabled(enabled);
    ui->spinBox_audioFreqStart->setEnabled(enabled);
    ui->spinBox_audioFreqEnd->setEnabled(enabled);
    ui->doubleSpinBox_audioDuration->setEnabled(enabled);
    ui->doubleSpinBox_audioOffset->setEnabled(enabled);
    ui->comboBox_audioChirp->setEnabled(enabled);
}


void MainWindow::setupRun()
{
    int stateColormap = ui->checkBox_viewColormap->checkState();
    int stateSaturation = ui->checkBox_viewSaturation->checkState();
    double timeIdle = ui->doubleSpinBox_trialTimeIdle->value();
    double timeBaseline = ui->doubleSpinBox_trialTimeBaseline->value();
    double timePreCue = ui->doubleSpinBox_trialTimePreCue->value();
    double timePostCue = ui->doubleSpinBox_trialTimePostCue->value();
    double timeSignal = ui->doubleSpinBox_trialTimeSignal->value();
    double valueThreshold = ui->doubleSpinBox_threshold->value();

    emit ui->checkBox_viewColormap->stateChanged(stateColormap);
    emit ui->checkBox_viewSaturation->stateChanged(stateSaturation);
    emit ui->doubleSpinBox_trialTimeIdle->valueChanged(timeIdle);
    emit ui->doubleSpinBox_trialTimeBaseline->valueChanged(timeBaseline);
    emit ui->doubleSpinBox_trialTimePreCue->valueChanged(timePreCue);
    emit ui->doubleSpinBox_trialTimePostCue->valueChanged(timePostCue);
    emit ui->doubleSpinBox_trialTimeSignal->valueChanged(timeSignal);
    emit ui->doubleSpinBox_threshold->valueChanged(valueThreshold);
}


void MainWindow::setupExport()
{
    // set defaul location
    m_exportPath = QStandardPaths::writableLocation(QStandardPaths::StandardLocation::DocumentsLocation);
    if (!QDir(m_exportPath).exists("IntrinsicImaging"))
        QDir(m_exportPath).mkdir("IntrinsicImaging");
    m_exportPath = QDir(m_exportPath).filePath("IntrinsicImaging");

    QString session = QDateTime::currentDateTime().toString("yyyyMMdd_hhmm");
    if (!QDir(m_exportPath).exists(session))
        QDir(m_exportPath).mkdir(session);
    m_exportPath = QDir(m_exportPath).filePath(session);

    ui->widget_eventLog->setExport(m_exportPath, "IntrinsicImaging");
    m_exporter->setExportPath(m_exportPath);
    emit notify("grey", "export path set: " + m_exportPath);
}

void MainWindow::configureSpinBox(QSpinBox *spinBox, int valueMin, int valueMax, int valueStep, int value)
{
    spinBox->setMinimum(valueMin);
    spinBox->setMaximum(valueMax);
    spinBox->setSingleStep(valueStep);
    spinBox->setValue(value);
}


void MainWindow::configureSpinBox(QDoubleSpinBox *spinBox, qreal valueMin, qreal valueMax, qreal valueStep, qreal value)
{
    spinBox->setMinimum(valueMin);
    spinBox->setMaximum(valueMax);
    spinBox->setSingleStep(valueStep);
    spinBox->setValue(value);
}



void MainWindow::on_checkBox_audioExtern_stateChanged(int state)
{
    if (state != Qt::CheckState::Checked)
        return;
}


void MainWindow::on_checkBox_audioPulse_stateChanged(int state)
{
    if (state == Qt::CheckState::Checked)
        setEnabledAudioUI(true);

    else if (state == Qt::CheckState::Unchecked)
        setEnabledAudioUI(false);
}

void MainWindow::on_pushButton_play_clicked()
{

    setupAudio();
    emit audioPlay();
}


void MainWindow::on_pushButton_start_clicked()
{
    // export start frame
    emit takeSnapshot("IntrinsicImaging_start");

    // setup audio
    setupAudio();

    // setup engine
    setupRun();

    // start
    emit notify("green", "IntrinsicEngine: start");
    emit engineStart();

    // reset timers
    ui->lineEdit_trialTime->setText("00:00:00");
    m_timerElapsed->start(1000);
    m_elapsedSeconds = 0;
}


void MainWindow::on_pushButton_stop_clicked()
{
    // export last frame
    emit takeSnapshot("IntrinsicImaging_stop");

    emit audioStop();
    emit engineStop();
    m_timerElapsed->stop();
    emit notify("green", "IntrinsicEngine: stop");
}

void MainWindow::on_pushButton_stream_clicked()
{
    emit cameraStart();
}


void MainWindow::on_pushButton_grab_clicked()
{
    emit takeSnapshot("IntrinsicImaging_grab");
}


void MainWindow::on_doubleSpinBox_threshold_valueChanged(double value)
{
    int valueSlider = static_cast<int>(value * 100);
    ui->horizontalSlider_threshold->blockSignals(true);
    ui->horizontalSlider_threshold->setValue(valueSlider);
    ui->horizontalSlider_threshold->blockSignals(false);
}

void MainWindow::on_horizontalSlider_threshold_valueChanged(int value)
{
    double valueSpinBox = value / 100.0;
    //ui->doubleSpinBox_threshold->blockSignals(true);
    ui->doubleSpinBox_threshold->setValue(valueSpinBox);
    //ui->doubleSpinBox_threshold->blockSignals(false);
}

void MainWindow::on_timerElapsed()
{
    m_elapsedSeconds++;
    ui->lineEdit_trialTime->setText(QDateTime::fromTime_t(m_elapsedSeconds).toUTC().toString("hh:mm:ss"));
}

void MainWindow::on_frameRateUpdate(const QString &report)
{
    ui->statusbar->showMessage(report, 2100);
}
