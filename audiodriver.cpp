#include "audiodriver.h"

const int SAMPLE_RATE = 44100;
const qreal FREQUENCY_MIN = 100.0;
const qreal FREQUENCY_MAX = 20000.0;

AudioDriver::AudioDriver(QObject *parent) : QObject(parent),
    m_indexPlayed(0),
    m_indexRepeat(0),
    m_pulseOffset(0),
    m_audio(nullptr),
    m_bufferBytes(new QByteArray()),
    m_bufferChirp(new QBuffer(m_bufferBytes, this)),
    m_timer(new QTimer(this))
{
    // set audio format
    QAudioFormat audioFormat;
    audioFormat.setCodec("audio/pcm");
    audioFormat.setByteOrder(QAudioFormat::LittleEndian);
    audioFormat.setSampleType(QAudioFormat::Float);
    audioFormat.setSampleSize(32);
    audioFormat.setChannelCount(1);
    audioFormat.setSampleRate(SAMPLE_RATE);

    // open audio output
    m_audio = new QAudioOutput(audioFormat, this);

    // set audio buffer
    m_bufferChirp->open(QIODevice::ReadOnly);


    // connect audio slot
    connect(m_audio, &QAudioOutput::stateChanged, this, &AudioDriver::on_audioStateChange);

    // prepare pulse train timer
    m_timer->setSingleShot(true);
    m_timer->setTimerType(Qt::TimerType::PreciseTimer);
    connect(m_timer, &QTimer::timeout, this, &AudioDriver::on_play);

    emit notify("gray", "AudioDriver: chirp composed");
}


AudioDriver::~AudioDriver()
{
    if (m_bufferChirp->isOpen())
        m_bufferChirp->close();
}


qreal AudioDriver::checkFrequencyRange(qreal freq)
{
    const qreal freqMin = FREQUENCY_MIN;
    const qreal freqMax = FREQUENCY_MAX;

    if(freq < freqMin)
        freq = freqMin;

    if (freqMax < freq)
        freq = freqMax;

    return freq;
}


qreal AudioDriver::closestFrequencyRange(qreal freq)
{
    const qreal freqMin = FREQUENCY_MIN;
    const qreal freqMax = FREQUENCY_MAX;
    const qreal freqMid = freqMin + (freqMax - freqMin) / 2.0;

    if (freq < freqMid)
        return freqMin;

    return freqMax;
}


void AudioDriver::generateChirp(QVector<qreal> &chirp, AudioDriver::ChirpType type,
                                int sampleRate, qreal duration, qreal phase,
                                qreal frequencyStart, qreal frequencyEnd)
{
    // define modulation callback
    typedef qreal (*DerivePhase)(qreal f, qreal k, qreal t);

    // default is pure tone chirp
    qreal f = frequencyStart;
    qreal k = 1.0;
    DerivePhase dPhase = &AudioDriver::derivePhasePure;

    // linear frequency modulation
    if (type == ChirpType::Linear) {
        k = (frequencyEnd - frequencyStart) / duration;
        dPhase = &AudioDriver::derivePhaseLinear;
    }
    // exponential frequency modulation
    else if (type == ChirpType::Exponential) {
        k = qPow(frequencyEnd / frequencyStart, 1 / duration);
        dPhase = &AudioDriver::derivePhaseExponential;
    }

    qreal dRate = 1.0 / sampleRate;
    int sampleCount = static_cast<int>(sampleRate * duration);
    for (int i = 0; i < sampleCount; ++i) {
        qreal t = dRate * (i + 1);
        qreal tone = qSin(phase + 2 * M_PI * dPhase(f, k, t));
        chirp.append(tone);
    }
}


qreal AudioDriver::derivePhasePure(qreal f, qreal k, qreal t)
{
    return k * f * t;
}


qreal AudioDriver::derivePhaseLinear(qreal f, qreal k, qreal t)
{
    return k * qPow(t, 2.0) / 2 + f * t;
}


qreal AudioDriver::derivePhaseExponential(qreal f, qreal k, qreal t)
{
    return f * (qPow(k, t) - 1) / qLn(k);
}


void AudioDriver::encodeChirpToBuffer(const QVector<qreal> &chirp)
{
    m_bufferBytes->clear();
    m_bufferChirp->seek(0);
    m_audio->reset();
    for (const qreal value : chirp) {
        float note = static_cast<float>(value);
        m_bufferBytes->append(reinterpret_cast<const char *>(&note), sizeof(note));
    }
}


void AudioDriver::on_play()
{
    // skip if active
    if (m_audio->state() == QAudio::ActiveState)
        return;

    // skip if buffer is empty
    if (m_bufferBytes->isEmpty() | m_bufferBytes->isNull())
        return;

    // rewind
    m_bufferChirp->seek(0);

    // play
    m_audio->start(m_bufferChirp);
    emit notify("gray", "AudioDriver: play");
}


void AudioDriver::on_stop()
{
    if (m_audio->state() == QAudio::ActiveState)
        m_audio->stop();

    if (m_timer->isActive())
        m_timer->stop();
}


void AudioDriver::on_chirpLoad(const QString &fileTune)
{
    if (m_audio->state() == QAudio::ActiveState)
        return;

    Q_UNUSED(fileTune);
}


void AudioDriver::on_chirpCompose(ChirpType type, int pulses,
                                  qreal duration, qreal offset,
                                  qreal freqStart, qreal freqEnd)
{
    if (m_audio->state() == QAudio::ActiveState)
        return;

    // check frequency
    freqStart = checkFrequencyRange(freqStart);
    freqEnd = checkFrequencyRange(freqEnd);


    // introduce ramp time
    qreal timeChirp = duration;
    qreal timeRamp = qMin(0.05, 0.05 * timeChirp);
    timeChirp -= 2 * timeRamp;

    // generate chirp
    // each chirp will have a pre and post ramp
    // the ramp starts / ends with the closest edge frequency
    QVector<qreal> chirp;
    generateChirp(chirp, ChirpType::Exponential, SAMPLE_RATE, timeRamp, 0.0, closestFrequencyRange(freqStart), freqStart);
    generateChirp(chirp, type, SAMPLE_RATE, timeChirp, chirp.last(), freqStart, freqEnd);
    generateChirp(chirp, ChirpType::Exponential, SAMPLE_RATE, timeRamp, chirp.last(), closestFrequencyRange(freqEnd), freqEnd);

    // fill buffer
    encodeChirpToBuffer(chirp);

    // prepare pulse train
    m_indexPlayed = 0;
    m_indexRepeat = pulses;
    m_pulseOffset = static_cast<int>(1000 * offset); // convert sec to msec
}


void AudioDriver::on_audioStateChange(QAudio::State state)
{
    if (state == QAudio::IdleState) {
        m_audio->stop();
        m_indexPlayed++;
        if (m_indexPlayed < m_indexRepeat) {
            m_timer->start(m_pulseOffset);
        }
        else {
            emit audioReady();
            m_indexPlayed = 0;
            emit notify("gray", "AudioDriver: done");
        }
    }
}
