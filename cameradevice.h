#ifndef CAMERADEVICE_H
#define CAMERADEVICE_H

#include <QObject>
#include <opencv2/opencv.hpp>

Q_DECLARE_METATYPE(cv::Mat)

class CameraDevice : public QObject
{
    Q_OBJECT

public:

    explicit CameraDevice(QObject *parent = nullptr) : QObject(parent),
        m_isDetected(false),
        m_isOpen(false),
        m_isStreaming(false)
    {
        qRegisterMetaType<cv::Mat>();
    }
    virtual ~CameraDevice() {}

    bool isDetected() const {return m_isDetected;}

private:
    bool m_isDetected;
    bool m_isOpen;
    bool m_isStreaming;

protected:
    bool isOpen() const {return m_isOpen;}
    bool isStreaming() const {return m_isStreaming;}

    void setIsDetected(bool flag) {m_isDetected = flag;}
    void setIsOpen(bool flag) {m_isOpen = flag;}
    void setIsStreaming(bool flag) {m_isStreaming = flag;}

public slots:
    virtual void on_open() = 0;
    virtual void on_close() = 0;
    virtual void on_start() = 0;
    virtual void on_stop() = 0;
    virtual void on_grab() = 0;

protected slots:
    virtual void on_capture() = 0;

signals:
    void notify(const QString &color, const QString &message, bool log = false);
    void frameReady(const cv::Mat &frame);
};
Q_DECLARE_INTERFACE(CameraDevice, "CameraDevice")


#endif // CAMERADEVICE_H
