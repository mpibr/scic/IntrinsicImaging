#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QThread>
#include <QMessageBox>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QTimer>
#include <QStandardPaths>
#include <QDir>

#include "eventlog.h"
#include "audiodriver.h"
#include "camerausb.h"
#include "cameraflir.h"
#include "intrinsicworker.h"
#include "intrinsicengine.h"
#include "intrinsicchart.h"
#include "frameexporter.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    AudioDriver *m_audio;
    IntrinsicWorker *m_iworker;
    IntrinsicEngine *m_iengine;
    CameraDevice *m_camera;
    CameraUsb *m_camera_usb;
    CameraFlir *m_camera_flir;
    FrameExporter *m_exporter;
    QThread *m_threadCamera;
    QThread *m_threadIWorker;
    QThread *m_threadExporter;
    QTimer *m_timerElapsed;
    int m_elapsedSeconds;
    QString m_exportPath;

    void closeEvent(QCloseEvent *event);

    void setupUiWidgets();
    void setupThreads();
    void setupConnections();
    void setupAudio();
    void setEnabledAudioUI(bool enabled);
    void setupRun();
    void setupExport();

    static void configureSpinBox(QSpinBox *spinBox,
                                 int valueMin, int valueMax,
                                 int valueStep, int value);
    static void configureSpinBox(QDoubleSpinBox *spinBox,
                                 qreal valueMin, qreal valueMax,
                                 qreal valueStep, qreal value);

private slots:    
    void on_checkBox_audioExtern_stateChanged(int state);
    void on_checkBox_audioPulse_stateChanged(int state);
    void on_pushButton_play_clicked();
    void on_pushButton_start_clicked();
    void on_pushButton_stop_clicked();
    void on_pushButton_stream_clicked();
    void on_pushButton_grab_clicked();
    void on_doubleSpinBox_threshold_valueChanged(double value); // !!! *** FIX COUPLING *** !!!
    void on_horizontalSlider_threshold_valueChanged(int value); // !!! *** FIX COUPLING *** !!!
    void on_timerElapsed();
    void on_frameRateUpdate(const QString &report);

signals:
    void notify(const QString &color, const QString &message, bool log = false);
    void audioLoad(const QString &fileAudio);
    void audioCompose(AudioDriver::ChirpType type, int pulses,
                      qreal duration, qreal offset,
                      qreal freqStart, qreal freqEnd);
    void audioPlay();
    void audioStop();
    void engineStart();
    void engineStop();
    void cameraOpen();
    void cameraClose();
    void cameraStart();
    void cameraStop();
    void flagHeatmap(bool flag);
    void flagSaturation(bool flag);
    void takeSnapshot(const QString &label);
};
#endif // MAINWINDOW_H
