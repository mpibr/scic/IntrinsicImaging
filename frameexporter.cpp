#include "frameexporter.h"

QString FrameExporter::configureFileName(const QString &name, const QString &ext)
{
    QString fileName;

    // check path
    if (!QDir(m_path).exists())
        QDir().mkdir(m_path);

    // compile name
    fileName = QDir(m_path).filePath(name + "_" + QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss") + ext);

    return fileName;
}


void FrameExporter::on_saveTif(const QString &name, const cv::Mat &frame)
{
    QString fileName = configureFileName(name, ".tiff");
    cv::imwrite(fileName.toStdString(), frame);
}


void FrameExporter::on_savePng(const QString &name, const QPixmap &pixmap)
{
    QString fileName = configureFileName(name, ".png");
    pixmap.save(fileName);
}
