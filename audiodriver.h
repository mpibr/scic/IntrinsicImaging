#ifndef AUDIODRIVER_H
#define AUDIODRIVER_H

#include <QObject>
#include <QAudioOutput>
#include <QBuffer>
#include <QVector>
#include <QtMath>
#include <QTimer>

class AudioDriver : public QObject
{
    Q_OBJECT

public:
    explicit AudioDriver(QObject *parent = nullptr);
    ~AudioDriver();

    enum ChirpType {Pure, Linear, Exponential, Extern};
    Q_ENUM(ChirpType)

private:
    int m_indexPlayed;
    int m_indexRepeat;
    int m_pulseOffset;
    QAudioOutput *m_audio;
    QByteArray *m_bufferBytes;
    QBuffer *m_bufferChirp;
    QTimer *m_timer;

    static qreal checkFrequencyRange(qreal freq);
    static qreal closestFrequencyRange(qreal freq);
    static void generateChirp(QVector<qreal> &chirp, ChirpType type,
                       int sampleRate, qreal duration, qreal phase,
                       qreal frequencyStart, qreal frequencyEnd);
    static qreal derivePhasePure(qreal f, qreal k, qreal t);
    static qreal derivePhaseLinear(qreal f, qreal k, qreal t);
    static qreal derivePhaseExponential(qreal f, qreal k, qreal t);

    void encodeChirpToBuffer(const QVector<qreal> &chirp);

public slots:
    void on_play();
    void on_stop();
    void on_chirpLoad(const QString &fileTune);
    void on_chirpCompose(ChirpType type, int pulses,
                        qreal duration, qreal offset,
                        qreal freqStart, qreal freqEnd);

private slots:
    void on_audioStateChange(QAudio::State state);

signals:
    void notify(const QString &color, const QString &message, bool log = false);
    void audioReady();
};

#endif // AUDIODRIVER_H
