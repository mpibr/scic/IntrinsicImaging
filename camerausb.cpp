#include "camerausb.h"

CameraUsb::~CameraUsb()
{
    if (m_frameCapture) {
        if (m_frameCapture->isOpened())
            m_frameCapture->release();
        delete m_frameCapture;
    }
}


void CameraUsb::on_open()
{
    // check status
    if (isOpen())
        return;

    if (isStreaming())
        return;

    // open frame capture
    m_frameCapture = new cv::VideoCapture(0);
    if (!m_frameCapture->isOpened()) {
        emit notify("red", "CameraUsb::Error, failed to open camera device.");
        return;
    }

    // update status
    setIsDetected(true);
    setIsOpen(true);
}


void CameraUsb::on_close()
{
    setIsStreaming(false);
    setIsOpen(false);
}


void CameraUsb::on_start()
{
    // check status
    if (!isOpen())
        return;

    if (isStreaming())
        return;

    setIsStreaming(true);
    QMetaObject::invokeMethod(this, "on_capture", Qt::QueuedConnection);
}


void CameraUsb::on_stop()
{
    // check status
    if (!isOpen())
        return;

    setIsStreaming(false);
}


void CameraUsb::on_grab()
{
    // check status
    if (!isOpen())
        return;

    setIsStreaming(false);
    QMetaObject::invokeMethod(this, "on_capture", Qt::QueuedConnection);
}


void CameraUsb::on_capture()
{
    cv::Mat frame;
    if (!m_frameCapture->read(frame)) {
        emit notify("red", "CameraUsb::Error, failed to acquire frame.");
        setIsStreaming(false);
        return;
    }

    cv::Mat frame8bit;
    cv::cvtColor(frame, frame8bit, cv::COLOR_BGR2GRAY);

    emit frameReady(frame8bit);

    // schedule next capture
    if (isStreaming())
        QMetaObject::invokeMethod(this, "on_capture", Qt::QueuedConnection);
}
