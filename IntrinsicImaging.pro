QT       += core gui multimedia charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
CONFIG+=sdk_no_version_check # silence 10.15 SDK warning

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    audiodriver.cpp \
    cameraflir.cpp \
    camerausb.cpp \
    eventlog.cpp \
    frameexporter.cpp \
    intrinsicchart.cpp \
    intrinsicengine.cpp \
    intrinsicviewer.cpp \
    intrinsicworker.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    audiodriver.h \
    cameradevice.h \
    cameraflir.h \
    camerausb.h \
    eventlog.h \
    frameexporter.h \
    intrinsicchart.h \
    intrinsicengine.h \
    intrinsicviewer.h \
    intrinsicworker.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# add external libraries
win32 {

    # no min max macros via compiler flag
    # NOMINMAX keeps windows.h from defining "min" and "max" via windef.h.
    # This avoids conflicts with the C++ standard library.
    QMAKE_CXXFLAGS *= /DNOMINMAX

    # add to PATH WindowsKits folder

    # opencv
    OPENCV_PATH = "D:\\Developer\\Library\\opencv\\build"
    CONFIG(release, debug|release): LIBS += -L$$OPENCV_PATH/x64/vc15/lib/ -lopencv_world343
    CONFIG(debug, debug|release): LIBS += -L$$OPENCV_PATH/x64/vc15/lib/ -lopencv_world343d
    INCLUDEPATH += $$OPENCV_PATH/include
    DEPENDPATH += $$OPENCV_PATH/include

    # spinnaker
    SPINNAKER_PATH = "C:\Program Files\FLIR Systems\Spinnaker"
    CONFIG(release, debug|release): LIBS += -L$$SPINNAKER_PATH/lib64/vs2015 -lSpinnaker_v140
    CONFIG(debug, debug|release): LIBS += -L$$SPINNAKER_PATH/lib64/vs2015 -lSpinnakerd_v140
    INCLUDEPATH += $$SPINNAKER_PATH/include

    # add GENICAM_LOG_CONFIG_V3_0 enviromental variable
    # default file should be
    # C:\Program Files\Point Grey Research\Spinnaker\log\config
}

macx {

    #opencv
    QT_CONFIG -= no-pkg-config
    CONFIG += link_pkgconfig
    PKGCONFIG += opencv4

    # JPEG
    #LIBS += -L/usr/local/Cellar/jpeg/9d/lib -ljpeg
    #INCLUDEPATH += /usr/local/Cellar/jpeg/9d/include

    # spinnaker
    LIBS += -L/usr/local/lib/ -lSpinnaker
    INCLUDEPATH += /usr/local/include/spinnaker/

}
