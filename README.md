# IntrinsicImaging

## About
detect intrinsic signal based on regular auditory stimulus

* for audio generation/play QtMultimedia and [QtAudioOutput](https://doc.qt.io/qt-5/qaudiooutput.html)

* camera acqusition is done by FLIR camera with C++ SDK [Spinnaker](https://www.flir.com/products/spinnaker-sdk/)

* intrinsic signal is calculated based on truncated difference method described by [Turley et.al.](https://pubmed.ncbi.nlm.nih.gov//28775255/)

## Instalation

download latest [release](https://public.brain.mpg.de/scic/releases/) for Windows7

## Work in progress
Current version caps the viewer frame rate to 10 frames per second, where intrinsic signal is detected on full frame rate.
Full frame visualisation will be possible with the integration of OpenGL and [QOpenGLWidget](https://doc.qt.io/qt-5/qopenglwidget.html)


