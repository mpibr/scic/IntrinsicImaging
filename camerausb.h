#ifndef CAMERAUSB_H
#define CAMERAUSB_H

#include <QObject>
#include "cameradevice.h"

class CameraUsb : public CameraDevice
{
    Q_OBJECT
    Q_INTERFACES(CameraDevice)

public:
    explicit CameraUsb(CameraDevice *parent = nullptr) :
        CameraDevice(parent),
        m_frameCapture(nullptr) {}
    ~CameraUsb() override;

private:
    cv::VideoCapture *m_frameCapture;

public slots:
    void on_open() override;
    void on_close() override;
    void on_start() override;
    void on_stop() override;
    void on_grab() override;

protected slots:
    void on_capture() override;
};

#endif // CAMERAUSB_H
