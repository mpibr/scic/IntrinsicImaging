#ifndef EVENTLOG_H
#define EVENTLOG_H

#include <QObject>
#include <QWidget>
#include <QCoreApplication>
#include <QPlainTextEdit>
#include <QScrollBar>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QTextStream>

class EventLog : public QPlainTextEdit
{
    Q_OBJECT

public:
    explicit EventLog(QWidget *parent = nullptr);
    ~EventLog();
    void setExport(const QString &path, const QString &tag);

private:
    bool m_isExportEnabled;
    QFile *m_logFile;
    QTextStream *m_logStream;

public slots:
    void on_notify(const QString &color, const QString &message, bool log = false);
};

#endif // EVENTLOG_H
