#include "intrinsicengine.h"

const int statesCount = 8;
const int scaleSecToMSec = 1000;

IntrinsicEngine::IntrinsicEngine(QObject *parent) : QObject(parent),
    m_state(EngineState::Viewer),
    m_engine(new QStateMachine(this)),
    m_timerEngine(new QTimer(this))
{
    // allocate container
    m_duration.fill(0, statesCount);

    // timers definition
    m_timerEngine->setSingleShot(true);

    // state machine definition
    QState *state_viewer = new QState(m_engine);
    QState *state_run = new QState(m_engine);
    QState *state_idle = new QState(state_run);
    QState *state_baseline = new QState(state_run);
    QState *state_precue = new QState(state_run);
    QState *state_cue = new QState(state_run);
    QState *state_postcue = new QState(state_run);
    QState *state_signal = new QState(state_run);

    state_viewer->addTransition(this, &IntrinsicEngine::engineStart, state_run);
    state_run->addTransition(this, &IntrinsicEngine::engineStop, state_viewer);
    state_idle->addTransition(m_timerEngine, &QTimer::timeout, state_baseline);
    state_baseline->addTransition(m_timerEngine, &QTimer::timeout, state_precue);
    state_precue->addTransition(m_timerEngine, &QTimer::timeout, state_cue);
    state_cue->addTransition(this, &IntrinsicEngine::cueReady, state_postcue);
    state_postcue->addTransition(m_timerEngine, &QTimer::timeout, state_signal);
    state_signal->addTransition(m_timerEngine, &QTimer::timeout, state_idle);

    // connect entered
    connect(state_viewer, &QState::entered, [this](){m_state = EngineState::Viewer;});
    connect(state_run, &QState::entered, [this](){m_state = EngineState::Run;});
    connect(state_idle, &QState::entered, [this](){m_state = EngineState::Idle; configureState();});
    connect(state_baseline, &QState::entered, [this](){m_state = EngineState::Baseline; configureState(); emit flagBaseline(true);});
    connect(state_precue, &QState::entered, [this](){m_state = EngineState::PreCue; configureState();});
    connect(state_cue, &QState::entered, [this](){m_state = EngineState::Cue; emit cuePlay(); emit flagCue(true);});
    connect(state_postcue, &QState::entered, [this](){m_state = EngineState::PostCue; configureState();});
    connect(state_signal, &QState::entered, [this](){m_state = EngineState::Signal; configureState(); emit flagSignal(true);});

    // connect exit
    connect(state_baseline, &QState::exited, [this](){emit flagBaseline(false);});
    connect(state_cue, &QState::exited, [this](){emit flagCue(false);});
    connect(state_signal, &QState::exited, [this](){
        emit flagSignal(false);
        emit trialReady();
        emit notify("orange", "IntrinsiscState: Trial done", true);
    });

    // state machine hierarchy
    state_run->setInitialState(state_idle);
    m_engine->setInitialState(state_viewer);
    m_engine->start();
}


IntrinsicEngine::~IntrinsicEngine()
{
    if (m_timerEngine->isActive())
        m_timerEngine->stop();

    if (m_engine->isRunning())
        m_engine->stop();
}


int IntrinsicEngine::convertSecondsToMSeconds(double timeSec)
{
    return static_cast<int>(scaleSecToMSec * timeSec);
}


void IntrinsicEngine::configureState()
{
    // dispatch label
    QString stateLabel = QVariant::fromValue(m_state).toString();
    emit notify("gray", "IntrinsicState: " + stateLabel, true);

    // start state duration
    int stateIndex = QVariant::fromValue(m_state).toInt();
    int timerValue = m_duration[stateIndex];
    if (timerValue > 0)
        m_timerEngine->start(timerValue);
}

void IntrinsicEngine::on_setTimeIdle(double timeSec)
{
    int indexState = QVariant::fromValue(EngineState::Idle).toInt();
    m_duration[indexState] = convertSecondsToMSeconds(timeSec);
}


void IntrinsicEngine::on_setTimeBaseline(double timeSec)
{
    int indexState = QVariant::fromValue(EngineState::Baseline).toInt();
    m_duration[indexState] = convertSecondsToMSeconds(timeSec);
}


void IntrinsicEngine::on_setTimePreCue(double timeSec)
{
    int indexState = QVariant::fromValue(EngineState::PreCue).toInt();
    m_duration[indexState] = convertSecondsToMSeconds(timeSec);
}


void IntrinsicEngine::on_setTimePostCue(double timeSec)
{
    int indexState = QVariant::fromValue(EngineState::PostCue).toInt();
    m_duration[indexState] = convertSecondsToMSeconds(timeSec);
}


void IntrinsicEngine::on_setTimeSignal(double timeSec)
{
    int indexState = QVariant::fromValue(EngineState::Signal).toInt();
    m_duration[indexState] = convertSecondsToMSeconds(timeSec);
}
