#ifndef CAMERAFLIR_H
#define CAMERAFLIR_H

#include <QObject>
#include "opencv2/opencv.hpp"
#include "Spinnaker.h"
#include "SpinGenApi/SpinnakerGenApi.h"
#include "cameradevice.h"

// add explicit declaration of template class Spinnaker::BasePtr
extern template class Spinnaker::BasePtr<Spinnaker::ISystem, Spinnaker::ISystem>; // Spinnaker::SystemPtr
extern template class Spinnaker::BasePtr<Spinnaker::Camera, Spinnaker::ICameraBase>; // Spinnaker::CameraPtr
extern template class Spinnaker::BasePtr<Spinnaker::IImage, Spinnaker::IImage>; // Spinnaker::ImagePtr

class CameraFlir : public CameraDevice
{
    Q_OBJECT
    Q_INTERFACES(CameraDevice)

public:
    explicit CameraFlir(CameraDevice *parent = nullptr);
    ~CameraFlir() override;

private:
    Spinnaker::SystemPtr m_system;
    Spinnaker::CameraList m_cameraList;
    Spinnaker::CameraPtr m_camera;

    bool isNodeReadable(Spinnaker::GenApi::INode *node);
    bool isNodeWritable(Spinnaker::GenApi::INode *node);

    void setNodeBool(const Spinnaker::GenApi::INodeMap &nodeMap,
                     const QString &nodeLabel, const bool flag);
    void setNodeEnum(const Spinnaker::GenApi::INodeMap &nodeMap,
                     const QString &nodeLabel, const QString &value);
    void setNodeInt(const Spinnaker::GenApi::INodeMap &nodeMap,
                    const QString &nodeLabel, const int64_t value);

    void deviceInfo();
    void dispatchFrame(Spinnaker::ImagePtr frame);

public slots:
    void on_open() override;
    void on_close() override;
    void on_start() override;
    void on_stop() override;
    void on_grab() override;

private slots:
    void on_capture() override;
};

#endif // CAMERAFLIR_H
