% testWidgets
clc;
clear variables;
close all;


qryFile = '/Users/tushevg/Desktop/20160815-134033_IntrinsicExport/20160815-134033_IntrinsicExport_background.tif';
img = imread(qryFile, 1);

% software binning
scale = 2;
kernel = ones(scale)./scale;




%{
tic
x = conv2(double(tmp),kernel,'same');
toc
%}

%{
tmp = double(img);
h = fspecial('gaussian', size(tmp),max(size(tmp))/8);
hMax = max(h(:));
hMin = min(h(:));
h = (h - hMin) ./ (hMax - hMin);




imgMax = max(tmp(:));
imgMin = min(tmp(:));
imgScaled = (tmp - imgMin) ./ (imgMax - imgMin);
alpha = h .* imgScaled;

mask = alpha > 0.1;

out = repmat(img, 1, 1, 3);
patchColor = [0,255,0];

for c = 1 : 3
    
    tmp = out(:,:,c);
    tmp(mask) = patchColor(c) * alpha(mask);
    out(:,:,c) = tmp;
    
end

imwrite(out,'/Users/tushevg/Desktop/test.png');
%}