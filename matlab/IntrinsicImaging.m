classdef IntrinsicImaging < handle
    %INTRINSICIMAGING 
    % 
    % description
    
    properties (Access = private, Hidden = true)
        
        folder
        name
        duration
        
    end
    
    %% --- ui handles --- %%
    properties (Access = private, Hidden = true)
        
        ui_parent
        
        widget_videoAcquisition
        widget_audioStimuli
        widget_intrinsicAnalysis
        
    end
    
    %% --- constant properties --- %%
    properties (Constant = true, Access = private, Hidden = true)
        
        UIWINDOW_SIZE = [1, 1, 266, 597];
        BACKGROUND_COLOR = [1, 1, 1];
        
    end
    
    %% --- constructor / destructor --- %%
    methods
        
        function obj = IntrinsicImaging()
            %INTRINSICIMAGING class constructor
            
            % render user interface
            obj.renderUserInterface();
            
            % introduce trigger listener
            if isa(obj.widget_intrinsicAnalysis, 'WidgetIntrinsicAnalysis')
                
                addlistener(obj.widget_intrinsicAnalysis, 'event_start', @obj.fcnExperiment_start);
                addlistener(obj.widget_intrinsicAnalysis, 'event_trigger', @obj.fcnExperiment_trigger);
                addlistener(obj.widget_intrinsicAnalysis, 'event_stop', @obj.fcnExperiment_stop);
                
            end
            
        end
        
        function obj = dispose(obj)
            %DISPOSE class destructor
            
            % call widget destructor
            if isa(obj.widget_audioStimuli, 'WdigetAudioStimuli')
                obj.widget_audioStimuli.dispose();
            end
            
            if isa(obj.widget_videoAcquisition, 'WidgetVideoAcquisition')
                obj.widget_videoAcquisition.dispose();
            end
            
            if isa(obj.widget_intrinsicAnalysis, 'WidgetIntrinsicAnalysis')
                obj.widget_intrinsicAnalysis.dispose();
            end
            
            
            % delete figure
            if isgraphics(obj.ui_parent, 'figure')
                delete(obj.ui_parent);
            end
            
            delete(obj);
            
        end
        
        
        function obj = renderUserInterface(obj)
            %RENDERUSERINTERFACE 
            
            % render parent
            obj.ui_parent = figure(...
                    'Visible', 'on',...
                    'Tag', 'hIntrinsicImaging',...
                    'Name', 'Intrinsic Imaging',...
                    'MenuBar', 'none',...
                    'ToolBar', 'none',...
                    'NumberTitle', 'off',...
                    'Color', obj.BACKGROUND_COLOR,...
                    'Resize', 'off',...
                    'Units', 'pixels',...
                    'Position', obj.UIWINDOW_SIZE,...
                    'CloseRequestFcn', @obj.fcnCallback_closeUserInterface);
           movegui(obj.ui_parent, 'northwest');
           
           % render widgets
           ui_panel = uipanel(...
                      'Parent',obj.ui_parent,...
                      'BorderType', 'none',...
                      'Units', 'pixels',...
                      'Position',[6,493,256,99]);
           obj.widget_videoAcquisition = WidgetVideoAcquisition('Parent', ui_panel, 'Adaptor', 'ni');
           
           
           ui_panel = uipanel(...
                      'Parent',obj.ui_parent,...
                      'BorderType', 'none',...
                      'Units', 'pixels',...
                      'Position',[6,277,256,211]);
           obj.widget_audioStimuli = WidgetAudioStimuli('Parent', ui_panel);       
          
           
           ui_panel = uipanel(...
                      'Parent',obj.ui_parent,...
                      'BorderType', 'none',...
                      'Units', 'pixels',...
                      'Position',[6,5,256,267]);
           obj.widget_intrinsicAnalysis = WidgetIntrinsicAnalysis('Parent', ui_panel);
           
       
        end
        
        
    end
    
    %% --- user interface callbacks --- %%
    methods
        
        function obj = fcnCallback_closeUserInterface(obj, ~, ~)
            %FCNCALLBACK_CLOSEUSERINTERFACE
            
            obj.dispose();
            
        end
        
        function obj = fcnExperiment_start(obj, ~, ~)
            %FCNEXPERIMENT_START toggle UI components enable state
            
            % pass public properties
            obj.widget_intrinsicAnalysis.adaptor = obj.widget_videoAcquisition.adaptor;
            obj.widget_intrinsicAnalysis.resolution = obj.widget_videoAcquisition.resolution;
            obj.widget_intrinsicAnalysis.fps = obj.widget_videoAcquisition.fps;
            obj.widget_intrinsicAnalysis.bitdepth = obj.widget_videoAcquisition.bitdepth;
            obj.widget_intrinsicAnalysis.frame = obj.widget_videoAcquisition.snapshot;
            
            % check if frame was grabbed
            if isempty(obj.widget_intrinsicAnalysis.frame)
                
                warndlg('Reference frame is empty! Grab a frame first!','Empty Frame');
                
            else
                
                % handle widget enable property
                obj.widget_videoAcquisition.enable('off');
                obj.widget_audioStimuli.enable('off');
                
                % stop video stream
                obj.widget_videoAcquisition.close();
                stop(obj.widget_videoAcquisition.stream);
                
                % start intrinsic analysis
                obj.widget_intrinsicAnalysis.initialize();
            
            end
            
            
        end
        
        function obj = fcnExperiment_trigger(obj, ~, ~)
            %FCNEXPERIMENT_TRIGGER trigger audio stimuli
            
            obj.widget_audioStimuli.play();
            
        end
        
        
        function obj = fcnExperiment_stop(obj, ~, ~)
            %FCNEXPERIMENT_STOP toggle UI components enable state
            
            obj.widget_videoAcquisition.enable('on');
            obj.widget_audioStimuli.enable('on');
            obj.widget_videoAcquisition.snapshot = [];
            
        end
        
    end
    
end

