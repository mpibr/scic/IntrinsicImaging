% saveXFramesFromCam
clc
clear variables
close all

%{
vidrd = VideoReader('20160817-015556_testCamIO.avi');
vF = readFrame(vidrd);
delete(vidrd);

R = uint16(vF(:,:,1));
G = uint16(vF(:,:,2));

X = bitor(bitshift(G, 8),R);

%}
%{
x = 256;

r = uint8(bitand(x, 255));
g = uint8(bitand(bitshift(x, -8), 255));
%}


%
obj = testCameraRecording();

obj.start();
wait(obj.stream, Inf);
obj.dispose();
%}