classdef testCameraRecording < handle
    % testCameraRecording
    
    properties
        
        pathOut
        fileOut
        name
        stream
        thread
        writer
        vidout
        buffer
        trial
        tagdata
        
    end
    
    properties (Constant)
        
        FRAME_SKIP = 5;
        FRAME_COUNT = 8*30;
        TRIGGER_REPEAT = 4;
        
    end
    
    methods
        
        function obj = testCameraRecording()
            
            obj.trial = 0;
            
            %%% --- create writer --- %%%
            obj.name = 'testCamIO';
            obj.pathOut = '/Users/tushevg/Desktop';
            obj.fileOut = sprintf('%s%s%s_%s',...
                          obj.pathOut,...
                          filesep,...
                          datestr(now,'yyyymmdd-HHMMSS'),...
                          obj.name);
            %{          
            obj.writer = Tiff([obj.fileOut,'.tif'], 'w');
            obj.tagdata.Photometric = Tiff.Photometric.MinIsBlack;
            obj.tagdata.ImageLength = 720;
            obj.tagdata.ImageWidth = 1280;
            obj.tagdata.RowsPerStrip = 720;
            obj.tagdata.BitsPerSample = 16;
            obj.tagdata.SamplesPerPixel = 1;
            obj.tagdata.SampleFormat = Tiff.SampleFormat.UInt;
            obj.tagdata.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
            obj.tagdata.Software = 'IntrinsicImaging';
            obj.writer.setTag(obj.tagdata);
            %}
            
            %%% --- video writer --- %%%
            obj.vidout = VideoWriter([obj.fileOut,'.avi'],...
                                     'Grayscale AVI');
            
            %%% --- export thread --- %%%
            obj.thread = timer(...
                'ExecutionMode', 'singleShot',...
                'Name', 'ThreadVideoExport',...
                'TimerFcn', @obj.fcnThread_export,...
                'TasksToExecute', 1);
            
            %%% --- camera stream --- %%%
            obj.stream = videoinput('macvideo', 1);
            set(obj.stream,...
               'ReturnedColorSpace', 'grayscale',...
               'LoggingMode', 'memory',...
               'FramesPerTrigger', obj.FRAME_COUNT,...
               'FramesAcquiredFcnCount', obj.FRAME_COUNT,...
               'FramesAcquiredFcn', @obj.fcnCamera_testAcquisition,...
               'TriggerFrameDelay', obj.FRAME_SKIP,...
               'TriggerRepeat', obj.TRIGGER_REPEAT);
           triggerconfig(obj.stream, 'immediate');  

            
        end
        
        
        function obj = start(obj)
            % start acquisition
            
            % open writer
            open(obj.vidout);
            
            % start stream
            start(obj.stream);
            
        end
        
        
        function obj = dispose(obj)
            % remove object
            
            %close(obj.writer);
            close(obj.vidout);
            
            stop(obj.stream);
            delete(obj.stream);
            delete(obj);
            
        end
        
        
    end
    
    %% --- callbacks --- %%
    methods
        
        function obj = fcnCamera_testAcquisition(obj, ~, ~)
            % acquisition callback
            obj.trial = obj.trial + 1;
            
            framesAvailable = get(obj.stream, 'FramesAvailable');
            [frames, timeStamp] = getdata(obj.stream, framesAvailable);
            frames = double(squeeze(frames));
            
            % convert 8bit to 12bit
            framesMax = max(frames(:));
            framesMin = min(frames(:));
            framesScaled = (frames - framesMin) ./ (framesMax - framesMin);
            frames = uint16((2^12 - 1) .* framesScaled);
            %}
            
            fprintf('%d :: in-buffer: %d\treturned: %d\tfps %.2f\n',...
                    obj.trial,...
                    framesAvailable,...
                    size(frames,3),...
                    1/mean(diff(timeStamp)));
            
            obj.buffer = frames;
            
            start(obj.thread);
            
        end
        
        
        function obj = fcnThread_export(obj, ~, ~)
            % export callback
            
            tic
            
            framesCount = size(obj.buffer, 3);
            for f = 1 : framesCount
                
                smallBits = uint8(bitand(obj.buffer(:,:,f), 255));
                largeBits = uint8(bitand(bitshift(obj.buffer(:,:,f),-8), 255));
                writeVideo(obj.vidout, smallBits);
                writeVideo(obj.vidout, largeBits);
                
            end
            
            %{
            if obj.trial == 1
                obj.writer.write(obj.buffer(:,:,1));
                
                writeVideo(obj.vidout, obj.buffer(:,:,1));
                startLoop = 2;
            else
                startLoop = 1;
            end
            
            framesCount = size(obj.buffer, 3);
            for f = startLoop : framesCount
                
                writeVideo(obj.vidout, obj.buffer(:,:,f));
                obj.writer.writeDirectory();
                obj.writer.setTag(obj.tagdata);
                obj.writer.write(obj.buffer(:,:,f));
                
            end
            %}
            toc
        end
        
    end
    
end

