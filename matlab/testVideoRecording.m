% testVideoRecording
clc
%{
clear variables
close all


fps = 75;
width = 1024;
height = 1024;
window = 8;

frames = uint16((2^12 - 1) .* rand(height, width, fps *  window));
%}
clearvars -except frames fps width height window;
tic
idx = zeros(fps*window,1);
idx(1:4:end) = 1;
idx = cumsum(idx);
count = max(idx);
new = zeros(height,width, max(idx),'uint16');
for j = 1 : count
    A = mean(frames(:,:,idx==j),3);
end

toc
%{
export_writerVideo = VideoWriter('test.avi', 'Grayscale AVI');
set(export_writerVideo, 'FrameRate', fps);
open(export_writerVideo);

%%% time this %%%
tic

framesCount = size(frames, 3);

for k = 1 : framesCount
   
     % 16 bit format
     lowerBits = uint8(bitand(frames(:,:,k), 255));
     higherBits = uint8(bitand(bitshift(frames(:,:,k),-8), 255));


     writeVideo(export_writerVideo, lowerBits);
     writeVideo(export_writerVideo, higherBits);
    
end
toc


close(export_writerVideo);
%}