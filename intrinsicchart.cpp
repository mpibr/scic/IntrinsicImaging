#include "intrinsicchart.h"

const qreal XAXIS_MIN = 0.0;
const qreal XAXIS_MAX = 60.0;
const qreal YAXIS_MIN = 0.0;
const qreal YAXIS_MAX = 100.0;

const auto COLOR_BLUE = QColor(0, 162, 255);
const auto COLOR_GREEN = QColor(97, 216, 54);
const auto COLOR_GOLD = QColor(248, 186, 0);
const auto COLOR_RED = QColor(255, 38, 0);
//const auto COLOR_GRAY = QColor(95, 95, 95);

IntrinsicChart::IntrinsicChart(QWidget *parent) : QChartView(parent),
    m_isRunning(false),
    m_flagBaseline(false),
    m_flagCue(false),
    m_flagSignal(false),
    m_chart(new QChart()),
    m_xaxis(new QValueAxis(this)),
    m_yaxis(new QValueAxis(this)),
    m_seriesBaseline(new QLineSeries()),
    m_seriesCue(new QLineSeries()),
    m_seriesSignal(new QLineSeries()),
    m_seriesIntrinsic(new QSplineSeries()),
    m_timer(new QElapsedTimer())
{
    // axis properties
    QFont axisFont;
    axisFont.setWeight(QFont::Light);
    axisFont.setPointSize(10);

    m_xaxis->setLabelsFont(axisFont);
    m_xaxis->setLabelFormat("%d");
    m_xaxis->setTitleFont(axisFont);
    m_xaxis->setTitleText("time [sec]");
    m_xaxis->setRange(XAXIS_MIN, XAXIS_MAX);

    m_yaxis->setLabelsFont(axisFont);
    m_yaxis->setLabelFormat("%d");
    m_yaxis->setTitleFont(axisFont);
    m_yaxis->setTitleText("intensity [%]");
    m_yaxis->setRange(YAXIS_MIN, YAXIS_MAX);

    // chart properties
    m_chart->setMargins(QMargins(0,0,0,0));
    m_chart->setBackgroundRoundness(0);
    m_chart->addAxis(m_xaxis, Qt::AlignBottom);
    m_chart->addAxis(m_yaxis, Qt::AlignLeft);

    // series properties
    configureSeries(m_seriesBaseline, "baseline", COLOR_BLUE);
    configureSeries(m_seriesCue, "cue", COLOR_GREEN);
    configureSeries(m_seriesSignal, "signal", COLOR_GOLD);
    configureSeries(m_seriesIntrinsic, "intrinsic", COLOR_RED);

    this->setChart(m_chart);
    this->setRenderHint(QPainter::Antialiasing);
}


void IntrinsicChart::configureSeries(QLineSeries *series, const QString &name, const QColor &color)
{
    m_chart->addSeries(series);
    series->attachAxis(m_xaxis);
    series->attachAxis(m_yaxis);
    series->setName(name);
    series->setColor(color);
    series->setUseOpenGL(false);
}


void IntrinsicChart::updateXAxisRange(qreal xmin, qreal xmax)
{
    m_seriesBaseline->clear();
    m_seriesCue->clear();
    m_seriesSignal->clear();
    m_seriesIntrinsic->clear();
    m_xaxis->setRange(xmin, xmax);
}

void IntrinsicChart::on_start()
{
    m_isRunning = true;
    updateXAxisRange(XAXIS_MIN, XAXIS_MAX);
    m_timer->start();
}


void IntrinsicChart::on_stop()
{
    m_isRunning = false;
}

void IntrinsicChart::on_plotIntrinsic(qreal value)
{
    if (!m_isRunning)
        return;

    // set wave values
    double yValueBaseline = m_flagBaseline ? 100.0 : 0.0;
    double yValueCue = m_flagCue ? 100.0 : 0.0;
    double yValueSignal = m_flagSignal ? 100.0 : 0.0;

    // set x value
    qreal xValue = m_timer->elapsed() / 1000.0;

    // update x-axis
    if(xValue >= m_xaxis->max())
        updateXAxisRange(m_xaxis->max(), 2 * m_xaxis->max());

    // plot series
    m_seriesBaseline->append(xValue, yValueBaseline);
    m_seriesCue->append(xValue, yValueCue);
    m_seriesSignal->append(xValue, yValueSignal);
    m_seriesIntrinsic->append(xValue, 100.0 * value);
}
