#ifndef INTRINSICCHART_H
#define INTRINSICCHART_H

#include <QObject>
#include <QWidget>
#include <QChartView>
#include <QChart>
#include <QValueAxis>
#include <QLineSeries>
#include <QSplineSeries>
#include <QElapsedTimer>

QT_CHARTS_USE_NAMESPACE

class IntrinsicChart : public QChartView
{
    Q_OBJECT
public:
    explicit IntrinsicChart(QWidget *parent = nullptr);

private:
    bool m_isRunning;
    bool m_flagBaseline;
    bool m_flagCue;
    bool m_flagSignal;
    QChart *m_chart;
    QValueAxis *m_xaxis;
    QValueAxis *m_yaxis;
    QLineSeries *m_seriesBaseline;
    QLineSeries *m_seriesCue;
    QLineSeries *m_seriesSignal;
    //QSplineSeries *m_seriesIntrinsic;
    QLineSeries *m_seriesIntrinsic;
    QElapsedTimer *m_timer;

    void configureSeries(QLineSeries *series, const QString &name, const QColor &color);
    void updateXAxisRange(qreal xmin, qreal xmax);

public slots:
    void on_start();
    void on_stop();
    void on_setFlagBaseline(bool flag) {m_flagBaseline = flag;}
    void on_setFlagCue(bool flag) {m_flagCue = flag;}
    void on_setFlagSignal(bool flag) {m_flagSignal = flag;}
    void on_plotIntrinsic(qreal value);
};

#endif // INTRINSICCHART_H
