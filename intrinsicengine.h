#ifndef INTRINSICENGINE_H
#define INTRINSICENGINE_H

#include <QObject>
#include <QStateMachine>
#include <QTimer>
#include <QVector>

class IntrinsicEngine : public QObject
{
    Q_OBJECT

public:
    explicit IntrinsicEngine(QObject *parent = nullptr);
    ~IntrinsicEngine();

    enum EngineState {Viewer, Run, Idle, Baseline, PreCue, Cue, PostCue, Signal};
    Q_ENUM(EngineState);

private:
    EngineState m_state;
    QVector<int> m_duration;
    QStateMachine *m_engine;
    QTimer *m_timerEngine;

    static int convertSecondsToMSeconds(double timeSec);
    void configureState();

public slots:
    void on_setTimeIdle(double timeSec);
    void on_setTimeBaseline(double timeSec);
    void on_setTimePreCue(double timeSec);
    void on_setTimePostCue(double timeSec);
    void on_setTimeSignal(double timeSec);

signals:
    void engineStart();
    void engineStop();
    void cuePlay();
    void cueReady();
    void flagBaseline(bool flag);
    void flagSignal(bool flag);
    void flagCue(bool flag);
    void trialReady();
    void notify(const QString &color, const QString &message, bool log = false);
};

#endif // INTRINSICENGINE_H
