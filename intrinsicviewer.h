#ifndef INTRINSICVIEWER_H
#define INTRINSICVIEWER_H

#include <QObject>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QColor>
#include <QTimer>

#include "opencv2/opencv.hpp"

class IntrinsicViewer : public QGraphicsView
{
    Q_OBJECT

public:
    explicit IntrinsicViewer(QWidget *parent = nullptr);
    ~IntrinsicViewer();

private:
    QTimer *m_timerFrameRate;
    int m_sampleCount;
    int m_frameCount;
    int m_frameRows;
    int m_frameCols;
    double m_frameRate;
    QString m_frameFormat;
    QGraphicsScene m_scene;
    QGraphicsPixmapItem m_pixmap;
    QPainter m_painter;
    QVector<QRgb> m_colorTable_jet;
    QVector<QRgb> m_colorTable_red;

    void createColorTableJet();
    void createColorTableRed();
    void updatePixmapByMask(const cv::Mat &mask, const QVector<QRgb> &colorTable);

public slots:
    void on_frame(const cv::Mat &frame_cv);
    void on_maskSaturation(const cv::Mat &mask);
    void on_maskHeatmap(const cv::Mat &mask);
    void on_snapshot(const QString &name);

private slots:
    void on_sampleFrameRate();

signals:
    void reportFrameRate(const QString &report);
    void savePixmap(const QString &name, const QPixmap &pixmap);
};

#endif // INTRINSICVIEWER_H
