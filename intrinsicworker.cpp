#include "intrinsicworker.h"

const int kernelSize = 31;
const double kernelSigma = 5.0;
const double scale16BitTo8Bit = (1 / 257.0); // factor = 65535/255 = 257
const int viewerFrameRate = 100;

IntrinsicWorker::IntrinsicWorker(QObject *parent) : QObject(parent),
    m_flagRender(false),
    m_flagBaseline(false),
    m_flagSignal(false),
    m_flagHeatmap(false),
    m_flagSaturation(false),
    m_frameRows(0),
    m_frameCols(0),
    m_frameType(CV_16UC1),
    m_indexBuffer(0),
    m_indexSignal(0),
    m_indexTrial(0),
    m_threshold(0.5),
    m_timer(new QTimer(this))
{
    connect(m_timer, &QTimer::timeout, [this](){m_flagRender = true;});
    m_timer->start(viewerFrameRate);
}


IntrinsicWorker::~IntrinsicWorker()
{
    if (m_timer->isActive())
        m_timer->stop();
}


void IntrinsicWorker::accumulateBuffer(bool flag, const cv::Mat &frame, const cv::Mat &buffer, int &index)
{
    if (!flag) {
        index = 0;
        return;
    }

    double scaling = 1.0 / (double)(index + 1);
    index++;
    cv::Mat bufferUpdated;
    cv::addWeighted(buffer, (1.0 - scaling), frame, scaling, 0.0, bufferUpdated, -1);
    bufferUpdated.copyTo(buffer);
}


void IntrinsicWorker::updateHeatmap()
{
    cv::Mat bufferNorm;
    cv::normalize(m_bufferIntrinsic, bufferNorm, 0.0, 255.0, cv::NORM_MINMAX, CV_8UC1);
    cv::threshold(bufferNorm, m_bufferHeatmap, 255.0 * m_threshold, 255.0, cv::THRESH_TOZERO);
    cv::threshold(bufferNorm, m_bufferMask, 255.0 * m_threshold, 255.0, cv::THRESH_BINARY);
}


void IntrinsicWorker::on_setGaussKernel(int kSize, double kSigma)
{
    // kernel size should be odd number
    if (kSize % 2 == 0)
        kSize++;
    m_kernel = cv::getGaussianKernel(kSize, kSigma, CV_64F);
}


void IntrinsicWorker::on_start()
{
    // allocate buffers
    m_bufferBaseline = cv::Mat::zeros(m_frameRows, m_frameCols, m_frameType);
    m_bufferSignal = cv::Mat::zeros(m_frameRows, m_frameCols, m_frameType);
    m_bufferIntrinsic = cv::Mat::zeros(m_frameRows, m_frameCols, CV_16UC1);
    m_bufferHeatmap = cv::Mat::zeros(m_frameRows, m_frameCols, CV_8UC1);
    m_bufferMask = cv::Mat::zeros(m_frameRows, m_frameCols, CV_8UC1);

    // allocate kernel
    on_setGaussKernel(kernelSize, kernelSigma);

    // reset trial counter
    m_indexTrial = 0;
}


void IntrinsicWorker::on_trial()
{
    // update trial counter
    m_indexTrial++;

    // apply gaussian filter on buffers
    cv::Mat trialBufferBaseline;
    cv::sepFilter2D(m_bufferBaseline, trialBufferBaseline, -1,
                    m_kernel, m_kernel,
                    cv::Point(-1,-1), 0, cv::BORDER_DEFAULT);

    cv::Mat trialBufferSignal;
    cv::sepFilter2D(m_bufferSignal, trialBufferSignal, -1,
                    m_kernel, m_kernel,
                    cv::Point(-1, -1), 0, cv::BORDER_DEFAULT);

    // update intrinsic difference
    cv::Mat trialBufferDiff;
    cv::subtract(trialBufferBaseline, trialBufferSignal, trialBufferDiff, cv::noArray(), CV_16UC1);
    cv::Mat trialBufferIntrinsic;
    cv::add(trialBufferDiff, m_bufferIntrinsic, trialBufferIntrinsic, cv::noArray(), CV_16UC1);
    trialBufferIntrinsic.copyTo(m_bufferIntrinsic);

    // update heatmap
    updateHeatmap();

    // export results
    QString fileLabel = "IntrinsicImaging_trial" + QString::number(m_indexTrial);
    emit saveFrame(fileLabel + "_baseline", trialBufferBaseline);
    emit saveFrame(fileLabel + "_signal", trialBufferSignal);
    emit saveFrame(fileLabel + "_heatmap", m_bufferHeatmap);
}


void IntrinsicWorker::on_frame(const cv::Mat &frame)
{
    // skip non-grayscale image
    if (frame.channels() != 1)
        return;

    // set frame sizes
    m_frameRows = frame.rows;
    m_frameCols = frame.cols;
    m_frameType = frame.type();


    if (m_flagRender) {

        // decode frame to Grayscale8Bit
        cv::Mat frame8Bit = cv::Mat(frame);
        if (frame8Bit.type() == CV_16UC1)
            frame.convertTo(frame8Bit, CV_8UC1, scale16BitTo8Bit);
        emit frameView(frame8Bit);

        // view heatmap
        if (m_flagHeatmap)
            emit maskHeatmap(m_bufferHeatmap);

        // view saturation
        if (m_flagSaturation)
            emit maskSaturation(frame8Bit);

        // calculate mask averate
        cv::Scalar meanSignal = cv::mean(frame8Bit, m_bufferMask);
        emit plotIntrinsic(meanSignal.val[0] / 255.0);

        // reset flag
        m_flagRender = false;
    }

    // accumulate buffer
    accumulateBuffer(m_flagBaseline, frame, m_bufferBaseline, m_indexBuffer);
    accumulateBuffer(m_flagSignal, frame, m_bufferSignal, m_indexSignal);
}
