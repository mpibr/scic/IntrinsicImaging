#ifndef FRAMEEXPORTER_H
#define FRAMEEXPORTER_H

#include <QObject>
#include <QDir>
#include <QDateTime>
#include <QPixmap>

#include <opencv2/opencv.hpp>

class FrameExporter : public QObject
{
    Q_OBJECT
public:
    explicit FrameExporter(QObject *parent = nullptr) : QObject(parent), m_path("") {}
    ~FrameExporter() {};

    void setExportPath(const QString &path) {m_path = path;}

private:
    QString m_path;
    QString configureFileName(const QString &name, const QString &ext);

public slots:
    void on_saveTif(const QString &name, const cv::Mat &frame);
    void on_savePng(const QString &name, const QPixmap &pixmap);
};

#endif // FRAMEEXPORTER_H
