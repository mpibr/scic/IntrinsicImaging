#include "cameraflir.h"

CameraFlir::CameraFlir(CameraDevice *parent) : CameraDevice(parent),
    m_camera(nullptr)
{
    // get reference to system object
    m_system = Spinnaker::System::GetInstance();

    // retrieve list of cameras
    m_cameraList = m_system->GetCameras();

    // check if camera is available
    if (m_cameraList.GetSize() > 0) {
        m_camera = m_cameraList.GetByIndex(0);
        setIsDetected(true);
    }
}


CameraFlir::~CameraFlir()
{
    if (isOpen()) {
        if (m_camera->IsValid()) {
            if (m_camera->IsStreaming())
                m_camera->EndAcquisition();

            if (m_camera->IsInitialized())
                m_camera->DeInit();
        }
    }

    m_camera = nullptr;
    m_cameraList.Clear();
    m_system->ReleaseInstance();
}


bool CameraFlir::isNodeReadable(Spinnaker::GenApi::INode *node)
{
    QString name = QString(node->GetName());
    if (!Spinnaker::GenApi::IsAvailable(node)) {
        emit notify("red", "CameraFlir: error, " + name + " is not available.");
        return false;
    }

    if (!Spinnaker::GenApi::IsReadable(node)) {
        emit notify("red", "CameraFlir: error, " + name + " is not readable.");
        return false;
    }

    return true;
}


bool CameraFlir::isNodeWritable(Spinnaker::GenApi::INode *node)
{
    QString name = QString(node->GetName());
    if (!Spinnaker::GenApi::IsAvailable(node)) {
        emit notify("red", "CameraFlir: error, " + name + " is not available.");
        return false;
    }

    if (!Spinnaker::GenApi::IsWritable(node)) {
        emit notify("red", "CameraFlir: error, " + name + " is not writable.");
        return false;
    }

    return true;
}


void CameraFlir::setNodeBool(const Spinnaker::GenApi::INodeMap &nodeMap,
                             const QString &nodeLabel, const bool flag)
{
    Spinnaker::GenApi::CBooleanPtr ptrNodeBool = nodeMap.GetNode(nodeLabel.toStdString().c_str());
    if (!isNodeWritable(ptrNodeBool)) {
        emit notify("red", "CameraFlir: error unable to set " + nodeLabel + " to " + QString::number(flag));
        return;
    }

    ptrNodeBool->SetValue(flag);
}

void CameraFlir::setNodeEnum(const Spinnaker::GenApi::INodeMap &nodeMap, const QString &nodeLabel, const QString &value)
{
    // retrieve enumeration node from nodemap
    Spinnaker::GenApi::CEnumerationPtr ptrNodeEnum = nodeMap.GetNode(nodeLabel.toStdString().c_str());
    if (!isNodeWritable(ptrNodeEnum)) {
        emit notify("red", "CameraFlir: error, unable to set " + nodeLabel + " to " + value);
        return;
    }


    // retrieve entry node from enumeration node
    Spinnaker::GenApi::CEnumEntryPtr ptrNodeEnumEntry = ptrNodeEnum->GetEntryByName(value.toStdString().c_str());
    if (!isNodeReadable(ptrNodeEnumEntry)) {
        emit notify("red", "CameraFlir: error, unable to set " + nodeLabel + " to " + value + ", not readable.");
        return;
    }

    // retrieve integer value from entry node
    const int64_t valueEntry = ptrNodeEnumEntry->GetValue();

    // set integer value from entry node as new value of enumeration node
    ptrNodeEnum->SetIntValue(valueEntry);
}


void CameraFlir::setNodeInt(const Spinnaker::GenApi::INodeMap &nodeMap, const QString &nodeLabel, const int64_t value)
{
    Spinnaker::GenApi::CIntegerPtr ptrMode = nodeMap.GetNode(nodeLabel.toStdString().c_str());
    if (!isNodeWritable(ptrMode)) {
        emit notify("red", "CameraFlir: error, unable to set " + nodeLabel + " to " + QString::number(value));
        return;
    }

    auto valueMin = ptrMode->GetMin();
    auto valueMax = ptrMode->GetMax();
    if ((value < valueMin) || (valueMax < value)) {
        emit notify("red", "CameraFlir: error, unable to set " + nodeLabel + " to " +
                    QString::number(value) + ", outside range [" +
                    QString::number(valueMin) + ", " + QString::number(valueMax) + "]");
        return;
    }

    // set requested value
    ptrMode->SetValue(value);
}


void CameraFlir::deviceInfo()
{
    try {
        emit notify("gray", "Spinnaker: Camera Device Information");
        emit notify("gray", "Vendor " + QString(m_camera->DeviceVendorName.GetValue()));
        emit notify("gray", "Model " + QString(m_camera->DeviceModelName.GetValue()));
        emit notify("gray", "ID " + QString(m_camera->DeviceID.GetValue()));
        emit notify("gray", "SensorWidth " + QString::number(m_camera->SensorWidth.GetValue()));
        emit notify("gray", "SensorHeight " + QString::number(m_camera->SensorHeight.GetValue()));
        emit notify("gray", "FrameWidth " + QString::number(m_camera->Width.GetValue()));
        emit notify("gray", "FrameHeight " + QString::number(m_camera->Height.GetValue()));
        emit notify("gray", "PixelFormat " + QString(m_camera->PixelFormat.GetCurrentEntry()->GetSymbolic()));
        QString binningVerticalMode = QString(m_camera->BinningVerticalMode.GetCurrentEntry()->GetSymbolic()) + " x" +
                QString::number(m_camera->BinningVertical.GetValue());
        QString binningHorizontalMode = QString(m_camera->BinningHorizontalMode.GetCurrentEntry()->GetSymbolic()) + " x" +
                QString::number(m_camera->BinningHorizontal.GetValue());
        emit notify("gray", "BinningVertical " + binningVerticalMode);
        emit notify("gray", "BinningHorizontal " + binningHorizontalMode);
        emit notify("gray", "AcquisitionFrameRate " + QString::number(m_camera->AcquisitionFrameRate.GetValue()));
        emit notify("gray", "Temperature " + QString::number(m_camera->DeviceTemperature.GetValue()));
    }
    catch (Spinnaker::Exception& e) {
        emit notify("red", "Spinnaker: error, " + QString(e.what()));
    }
}


void CameraFlir::dispatchFrame(Spinnaker::ImagePtr frame)
{
    int padding_X = static_cast<int>(frame->GetXPadding());
    int padding_Y = static_cast<int>(frame->GetYPadding());
    int frame_W = static_cast<int>(frame->GetWidth());
    int frame_H = static_cast<int>(frame->GetHeight());

    cv::Mat frame_cv = cv::Mat(frame_H + padding_Y, frame_W + padding_X,
                               CV_16UC1, frame->GetData(), frame->GetStride());

    emit frameReady(frame_cv);
}

void CameraFlir::on_open()
{
    // check guards
    if (!isDetected()) {
        emit notify("red", "CameraFlir: error, camera is not detected.");
        return;
    }

    if (isOpen()) {
        emit notify("orange", "CameraFlir: warning, camera is open.");
        return;
    }

    if (isStreaming()) {
        emit notify("orange", "CameraFlir: warning, camera is streaming.");
        return;
    }


    try {
        // initialize camera
        m_camera->Init();
        setIsOpen(true);

        // configure camera
        Spinnaker::GenApi::INodeMap &nodeMap = m_camera->GetNodeMap();
        //setNodeBool(nodeMap, "IspEnable", false);
        setNodeEnum(nodeMap, "PixelFormat", "Mono16");
        setNodeEnum(nodeMap, "BinningHorizontalMode", "Average");
        setNodeEnum(nodeMap, "BinningVerticalMode", "Average");
        setNodeInt(nodeMap, "BinningHorizontal", 4);
        setNodeInt(nodeMap, "BinningVertical", 4);
        setNodeEnum(nodeMap,"AcquisitionMode", "Continuous");

        // display settings
        deviceInfo();

        // good to go
        if (m_camera->IsInitialized())
            emit notify("green", "CameraFlir: device is ready.");
    }
    catch (Spinnaker::Exception& e) {
        emit notify("red", "CameraFlir: error, " + QString(e.what()));
        return;
    }
}


void CameraFlir::on_close()
{
    // check guards
    if (!isDetected()) {
        emit notify("red", "CameraFlir: error, camera is not detected.");
        return;
    }

    if (!isOpen()) {
        emit notify("orange", "CameraFlir: warning, camera is already closed.");
        return;
    }

    // stop streaming
    setIsStreaming(false);
    if (m_camera->IsStreaming())
        m_camera->EndAcquisition();

    // close camera
    setIsOpen(false);
    if (m_camera->IsInitialized())
        m_camera->DeInit();
}


void CameraFlir::on_start()
{
    // check guards
    if (!isDetected()) {
        emit notify("red", "CameraFlir: error, camera is not detected.");
        return;
    }

    if (!isOpen()) {
        emit notify("orange", "CameraFlir: warning, camera is closed.");
        return;
    }

    if (isStreaming()) {
        emit notify("orange", "CameraFlir: warning, camera is streaming.");
        return;
    }

    // begin acquisition
    m_camera->BeginAcquisition();
    setIsStreaming(true);
    QMetaObject::invokeMethod(this, "on_capture", Qt::QueuedConnection);
}


void CameraFlir::on_stop()
{
    // check guards
    if (!isDetected()) {
        emit notify("red", "CameraFlir: error, camera is not detected.");
        return;
    }

    if (!isOpen()) {
        emit notify("orange", "CameraFlir: warning, camera is closed.");
        return;
    }

    if (!isStreaming()) {
        emit notify("orange", "CameraFlir: warning, camera is not streaming.");
        return;
    }

    setIsStreaming(false);
    if (m_camera->IsStreaming())
        m_camera->EndAcquisition();
}


void CameraFlir::on_grab()
{
    // check state
    if (!isDetected()) {
        emit notify("red", "CameraFlir: error, camera is not detected.");
        return;
    }

    if (!isOpen()) {
        emit notify("orange", "CameraFlir: warning, camera is closed.");
        return;
    }

    // begin acquisition if needed
    if (!m_camera->IsStreaming())
        m_camera->BeginAcquisition();

    // deactivate stream and capture frame
    setIsStreaming(false);
    QMetaObject::invokeMethod(this, "on_capture", Qt::QueuedConnection);

    // end acqusition
    m_camera->EndAcquisition();
}


void CameraFlir::on_capture()
{
    if (!m_camera->IsStreaming())
        return;

    try {
        // get next image
        Spinnaker::ImagePtr frame_raw = m_camera->GetNextImage();

        if (frame_raw->IsIncomplete()) {
            emit notify("orange", "Spinnaker: warning, incomplete frame acquired. " +
                        QString(Spinnaker::Image::GetImageStatusDescription(frame_raw->GetImageStatus())));
        }
        else {
            dispatchFrame(frame_raw);
        }

        // release memory
        frame_raw->Release();
    }
    catch (Spinnaker::Exception &e) {
        emit notify("red", "Spinnaker: error, caputre failed " + QString(e.what()));
        on_stop();
    }

    // schedule next capture
    if (isStreaming())
        QMetaObject::invokeMethod(this, "on_capture", Qt::QueuedConnection);
}
