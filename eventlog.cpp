#include "eventlog.h"

EventLog::EventLog(QWidget *parent) : QPlainTextEdit(parent)
  , m_isExportEnabled(false)
  , m_logFile(nullptr)
  , m_logStream(nullptr)
{
    QFont textFont;
    textFont.setPointSize(10);
    textFont.setWeight(QFont::Thin);
    this->setFont(textFont);
    this->setReadOnly(true);
}


EventLog::~EventLog()
{
    if (m_logFile != nullptr)
        m_logFile->close();
}


void EventLog::setExport(const QString &path, const QString &tag)
{
    QString timeStamp = QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss");
    QString fileName = "EventLog_" + timeStamp + "_" + tag + ".txt";

    QString pathName = path;
    if (path.isEmpty())
        pathName = QDir(QCoreApplication::applicationDirPath()).filePath("logs");

    QDir pathExport = QDir(pathName);
    if (!pathExport.exists())
        pathExport.mkdir(pathName);

    m_logFile = new QFile(pathExport.filePath(fileName));
    m_logFile->open(QIODevice::WriteOnly | QIODevice::Text);
    if (!m_logFile->isOpen() || !m_logFile->isWritable()) {
        m_isExportEnabled = false;
        QString errorMessage = "EventLog::error, failed to open log file " + m_logFile->fileName();
        on_notify("red", errorMessage);
        return;
    }
    m_logStream = new QTextStream(m_logFile);

    m_isExportEnabled = true;
}


void EventLog::on_notify(const QString &color, const QString &message, bool log)
{
    QBrush brush = QBrush(Qt::SolidPattern);
    brush.setColor(QColor(color));
    QTextCharFormat textFormat = this->currentCharFormat();
    textFormat.setForeground(brush);
    this->mergeCurrentCharFormat(textFormat);
    this->appendPlainText(message);
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->maximum());

    if (log && m_isExportEnabled) {
        QString timeStamp = QDateTime::currentDateTime().toString("[yyyyMMdd hh:mm:ss]");
        QString logMessage = timeStamp + " " + message;
        *m_logStream << logMessage << "\n";
    }
}
