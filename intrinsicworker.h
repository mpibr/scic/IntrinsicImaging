#ifndef INTRINSICWORKER_H
#define INTRINSICWORKER_H

#include <QObject>
#include <QTimer>
#include "opencv2/opencv.hpp"

class IntrinsicWorker : public QObject
{
    Q_OBJECT

public:
    explicit IntrinsicWorker(QObject *parent = nullptr);
    ~IntrinsicWorker();

private:
    bool m_flagRender;
    bool m_flagBaseline;
    bool m_flagSignal;
    bool m_flagHeatmap;
    bool m_flagSaturation;
    int m_frameRows;
    int m_frameCols;
    int m_frameType;
    int m_indexBuffer;
    int m_indexSignal;
    int m_indexTrial;
    double m_threshold;
    cv::Mat m_bufferBaseline;
    cv::Mat m_bufferSignal;
    cv::Mat m_bufferIntrinsic;
    cv::Mat m_bufferHeatmap;
    cv::Mat m_bufferMask;
    cv::Mat m_kernel;
    QTimer *m_timer;

    void accumulateBuffer(bool flag, const cv::Mat &frame, const cv::Mat &buffer, int &index);
    void updateHeatmap();

public slots:
    void on_setFlagBaseline(bool flag) {m_flagBaseline = flag;}
    void on_setFlagSignal(bool flag) {m_flagSignal = flag;}
    void on_setFlagHeatmap(bool flag) {m_flagHeatmap = flag;}
    void on_setFlagSaturation(bool flag) {m_flagSaturation = flag;}
    void on_setThreshold(double value) {m_threshold = value; updateHeatmap();}
    void on_setGaussKernel(int kSize, double kSigma);
    void on_start();
    void on_trial();
    void on_frame(const cv::Mat &frame);

signals:
    void frameView(const cv::Mat &frame);
    void maskSaturation(const cv::Mat &mask);
    void maskHeatmap(const cv::Mat &mask);
    void plotIntrinsic(qreal value);
    void saveFrame(const QString &name, const cv::Mat &frame);
};

#endif // INTRINSICWORKER_H
